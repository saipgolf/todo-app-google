const redis = require('ioredis');
const REDIS_PORT = process.env.REDIS_PORT || 6379;
const REDIS_HOST = process.env.REDIS_HOST || '10.226.202.35';
 
exports.get = async(req, res) => {
    try {
        if (!req.query.user) {
            res.status(400).send('User is not defined');  
        }

        var client = redis.createClient(REDIS_PORT, REDIS_HOST);
        var data = await client.lrange(req.query.user, 0, 1000);
        await client.disconnect();

        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end(JSON.stringify('user') + ':' + JSON.stringify(req.query.user) + ',' + JSON.stringify('todo') + ':' + JSON.stringify(data));
    }
    catch (err) {
        res.status(500).send(err.message);
    }
};