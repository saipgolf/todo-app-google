const redis = require('ioredis');
const REDIS_PORT = process.env.REDIS_PORT || 6379;
const REDIS_HOST = process.env.REDIS_HOST || '10.226.202.35';

exports.delete = async(req, res) => {
    try {
        if (!req) {
            res.status(400).send('No body input is not defined');  
        }
        if (!req.body.user) {
            res.status(400).send('User is not defined');  
        }

        var client = redis.createClient(REDIS_PORT, REDIS_HOST);
        await client.del(req.body.user);
        await client.disconnect();

        res.status(200).send();
    }
    catch (err) {
        res.status(500).send(err.message);
    }
};
