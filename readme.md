# Web API (todo-app) in Google Cloud Function
## Preparation of functions development in Google Cloud Platform

0. Open Google Cloud Platform https://console.cloud.google.com/
1. Enable Cloud Build API
2. Create project to host functions, database and connector
3. Create Serverless Virtual Private Cloud (VPC) access connector and attached in to projects default VPC network
4. Create Redis instance and attached in to projects default VPC network
5. Import the functions and attach to serverless VPC connector

## Example of usage:
curl -X POST -d '{"user":"user1","todo":"clean up"}' https://europe-west1-my-todo-app-313712.cloudfunctions.net/add-todo 

curl -X POST -d '{"user":"user1","todo":"wash dishes"}' https://europe-west1-my-todo-app-313712.cloudfunctions.net/add-todo

curl -X GET https://europe-west1-my-todo-app-313712.cloudfunctions.net/list-todo?user=user1

curl -X DELETE -d '{"user":"user1"}'  https://europe-west1-my-todo-app-313712.cloudfunctions.net/clear-todo

curl -X GET https://europe-west1-my-todo-app-313712.cloudfunctions.net/about

curl -X GET https://europe-west1-my-todo-app-313712.cloudfunctions.net/credit


